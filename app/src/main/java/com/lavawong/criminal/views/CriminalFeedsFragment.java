package com.lavawong.criminal.views;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lavawong.criminal.R;
import com.lavawong.criminal.components.LWFragment;
import com.lavawong.criminal.databinding.FragmentCriminalFeedsBinding;
import com.lavawong.criminal.model.Crime;
import com.lavawong.criminal.viewModel.CrimeListViewModel;

import java.util.ArrayList;

public class CriminalFeedsFragment extends LWFragment {

    private FragmentCriminalFeedsBinding binding;

    private CrimeListViewModel crimeListViewModel;

    private CrimeAdapter adapter;

    private Callbacks callbacks;

    interface Callbacks {
        public void onCrimeSelected(String crimeId);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        callbacks = (Callbacks) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callbacks = null;
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        super.onCreateView(inflater, container, savedInstanceState);
        crimeListViewModel = ViewModelProviders.of(this).get(CrimeListViewModel.class);
        binding = FragmentCriminalFeedsBinding.inflate(inflater, container, false);
        binding.crimeRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        updateUI();
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//
//        binding.buttonFirst.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                NavHostFragment.findNavController(CriminalFeedsFragment.this)
//                        .navigate(R.id.action_FeedsFragment_to_DetailFragment);
//            }
//        });

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private void updateUI() {
        ArrayList<Crime> crimes = crimeListViewModel.getCrimes();
        adapter = new CrimeAdapter(crimes);
        binding.crimeRecyclerView.setAdapter(adapter);
    }
    private abstract class CrimeHolder extends RecyclerView.ViewHolder {

        public CrimeHolder(@NonNull View itemView) {
            super(itemView);
        }

        public abstract void bind(Crime crime);
    }

    private class CommonHolder extends CrimeHolder implements View.OnClickListener {
        private final TextView titleTextView;
        private final TextView dateTextView;
        private Crime crime;

        public CommonHolder(@NonNull View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.crime_title);
            dateTextView = itemView.findViewById(R.id.crime_date);
            itemView.setOnClickListener(this);
        }

        /**
         * 将 Crime 的数据绑定到 Item 上
         * @param crime Item 对应的 Model 数据
         */
        public void bind(@NonNull Crime crime) {
            this.crime = crime;
            titleTextView.setText(crime.getTitle());
            dateTextView.setText(crime.getDate());
        }

        @Override
        public void onClick(View v) {
            Toast.makeText(v.getContext(), String.format("%s pressed!", crime.getTitle()), Toast.LENGTH_SHORT)
                    .show();
            callbacks.onCrimeSelected(crime.getUuid());
        }
    }

    private final class PoliceHolder extends CrimeHolder implements View.OnClickListener {
        private final TextView titleTextView;
        private final TextView dateTextView;
        private Crime crime;

        public PoliceHolder(@NonNull View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.crime_title);
            dateTextView = itemView.findViewById(R.id.crime_date);
            titleTextView.setOnClickListener(this);
            dateTextView.setOnClickListener(this);

            ImageButton requirePoliceBtn = itemView.findViewById(R.id.require_police_btn);
            requirePoliceBtn.setOnClickListener((View v)->{
                Toast.makeText(v.getContext(), String.format("%s 叫警察啊！！", crime.getTitle()), Toast.LENGTH_SHORT)
                        .show();
                callbacks.onCrimeSelected(crime.getUuid());
            });
        }

        /**
         * 将 Crime 的数据绑定到 Item 上
         * @param crime Item 对应的 Model 数据
         */
        @Override
        public void bind(@NonNull Crime crime) {
            this.crime = crime;
            titleTextView.setText(crime.getTitle());
            dateTextView.setText(crime.getDate());
        }

        @Override
        public void onClick(View v) {
            Toast.makeText(v.getContext(), String.format("%s pressed!", crime.getTitle()), Toast.LENGTH_SHORT)
                    .show();
        }
    }

    private final class CrimeAdapter extends RecyclerView.Adapter<CrimeHolder> {
        public static final int COMMON_VIEW = 1;
        public static final int POLICE_VIEW = 2;
        private final ArrayList<Crime> crimes;

        public CrimeAdapter(ArrayList<Crime> crimes) {
            this.crimes = crimes;
        }

        @Override
        public int getItemViewType(int position) {
            Crime crime = crimes.get(position);
            if (!crime.isRequiresPolice()) {
                return COMMON_VIEW;
            } else {
                return POLICE_VIEW;
            }
        }

        @NonNull
        @Override
        public CrimeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            int res;
            CrimeHolder viewHolder;
            switch (viewType) {
                case POLICE_VIEW: {
                    res = R.layout.police_item_crime;
                    View view = getLayoutInflater().inflate(res, parent, false);
                    viewHolder = new PoliceHolder(view);
                    break;
                }
                case COMMON_VIEW:
                default: {
                    res = R.layout.list_item_crime;
                    View view = getLayoutInflater().inflate(res, parent, false);
                    viewHolder = new CommonHolder(view);
                }
            }
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull CrimeHolder holder, int position) {
            Crime crime = crimes.get(position);
            holder.bind(crime);
        }

        @Override
        public int getItemCount() {
            return crimes != null ? crimes.size() : 0;
        }
    }

}