package com.lavawong.criminal.views;

import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.lavawong.criminal.R;
import com.lavawong.criminal.components.LWActivity;
import com.lavawong.criminal.databinding.ActivityMainBinding;
import com.lavawong.criminal.utils.ActivityUtil;

public class MainActivity extends LWActivity implements CriminalFeedsFragment.Callbacks {

    public static final String ARG_CRIME_ID = "crime_id";
    private AppBarConfiguration appBarConfiguration;
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        appBarConfiguration = new AppBarConfiguration.Builder(navController.getGraph()).build();
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);

        binding.addCriminalItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//                LWActivity activity = ActivityUtil.getInstance().getCurrentActivity();
//                FragmentManager fm = activity.getSupportFragmentManager();
//                Fragment fragment = fm.findFragmentById(R.id.FeedsFragment);
//                NavHostFragment.findNavController(fragment)
//                        .navigate(R.id.action_FeedsFragment_to_SaveFragment);
                NavController navController = Navigation.findNavController(
                        ActivityUtil.getInstance().getCurrentActivity(),
                        R.id.nav_host_fragment_content_main
                );
                navController.navigate(R.id.action_FeedsFragment_to_SaveFragment);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        return NavigationUI.navigateUp(navController, appBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public void onCrimeSelected(String crimeId) {
//        getSupportFragmentManager()
//                .beginTransaction()
//                .replace(R.id.FeedsFragment, new SaveCriminalFragment())
//                .addToBackStack(null)
//                .commit();
        NavController navController = Navigation.findNavController(
                ActivityUtil.getInstance().getCurrentActivity(),
                R.id.nav_host_fragment_content_main
        );
        Bundle args = new Bundle();
        args.putString(ARG_CRIME_ID, crimeId);
        navController.navigate(R.id.action_FeedsFragment_to_SaveFragment, args);

    }
}