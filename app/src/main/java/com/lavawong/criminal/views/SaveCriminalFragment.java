package com.lavawong.criminal.views;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.fragment.NavHostFragment;

import com.lavawong.criminal.R;
import com.lavawong.criminal.components.LWFragment;
import com.lavawong.criminal.databinding.FragmentSaveCriminalBinding;
import com.lavawong.criminal.model.Crime;
import com.lavawong.criminal.utils.ActivityUtil;

import java.util.Calendar;

public class SaveCriminalFragment extends LWFragment {

    private FragmentSaveCriminalBinding binding;

    private Crime crime;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            String crimeId = savedInstanceState.getString(MainActivity.ARG_CRIME_ID);
        }
        crime = new Crime();
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        binding = FragmentSaveCriminalBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindingTitleField(binding.crimeTitle);
        bindingSolvedField(binding.crimeSolved);
        bindingSaveCrime(binding.saveCrime);
        binding.crimeDate.setEnabled(false);

        binding.crimeTitle.setText(crime.getTitle());
        binding.crimeDate.setText(crime.getDate());
    }

    private void bindingSaveCrime(Button saveCrime) {
        saveCrime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO　保存逻辑
                Toast successTips = Toast.makeText(v.getContext(), "保存成功", Toast.LENGTH_SHORT);
                successTips.show();
                gotoFeedList();
            }
        });
    }

    private void gotoFeedList() {
        NavHostFragment.findNavController(SaveCriminalFragment.this)
                .navigate(R.id.action_SaveFragment_to_FeedsFragment);
    }

    private void bindingSolvedField(CheckBox crimeSolved) {
        crimeSolved.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                crime.setSolved(isChecked);
            }
        });
    }

    private void bindingDateField(Button crimeDate) {
        crimeDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void bindingTitleField(EditText crimeTitle) {
        crimeTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                crime.setTitle(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
