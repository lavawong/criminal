package com.lavawong.criminal.utils;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Room;

import com.lavawong.criminal.database.AppDatabase;
import com.lavawong.criminal.database.dao.UserDao;
import com.lavawong.criminal.database.entities.User;

import java.util.List;

/**
 * Activity工具类库
 */
public class ActivityUtil {
    private static ActivityUtil instance;

    private Activity currentActivity;

    private Application application;

    public AppDatabase getDb() {
        return db;
    }

    public void setDb(AppDatabase db) {
        this.db = db;
    }

    private AppDatabase db;

    public static ActivityUtil getInstance() {
        if (instance == null) {
            instance = new ActivityUtil();
        }
        return instance;
    }

    /**
     * 获取当前展示的Activity
     * @return Activity
     */
    public Activity getCurrentActivity() {
        return currentActivity;
    }
    /**
     * 设置当前展示的Activity
     * @param activity 当前展示的Activity
     */
    public void setCurrentActivity(@NonNull Activity activity) {
        currentActivity = activity;
    }

    /**
     * 获取当前项目的Application
     * @return Application
     */
    public Application getCurrentApplication() {
        return application;
    }

    /**
     * 设置当前项目的Application
     * @param app 当前项目的Application
     */
    public void setCurrentApplication(@NonNull Application app) {
        application = app;
    }

}
