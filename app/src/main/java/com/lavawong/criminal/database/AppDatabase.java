package com.lavawong.criminal.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.lavawong.criminal.database.dao.UserDao;
import com.lavawong.criminal.database.entities.User;

@Database(entities = {User.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract UserDao userDao();
}