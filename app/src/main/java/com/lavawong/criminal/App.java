package com.lavawong.criminal;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Room;

import com.lavawong.criminal.components.LWActivity;
import com.lavawong.criminal.database.AppDatabase;
import com.lavawong.criminal.database.dao.UserDao;
import com.lavawong.criminal.database.entities.User;
import com.lavawong.criminal.utils.ActivityUtil;

import java.util.List;

public class App extends Application {
    private App.AppActivityLifecycleCallbacks appActivityLifecycleCallbacks;

    @Override
    public void onCreate() {
        super.onCreate();
        initGetCurrentContext();
        initGetCurrentActivity();
        initDatabase();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        unregisterActivityLifecycleCallbacks(appActivityLifecycleCallbacks);
    }

    private void initGetCurrentContext() {
        ActivityUtil.getInstance().setCurrentApplication(this);
    }

    private void initGetCurrentActivity() {
        appActivityLifecycleCallbacks = new AppActivityLifecycleCallbacks();
        registerActivityLifecycleCallbacks(appActivityLifecycleCallbacks);
    }

    private void initDatabase() {

        Context ctx = this.getApplicationContext();
        AppDatabase db = Room.databaseBuilder(ctx, AppDatabase.class, "crime-database").allowMainThreadQueries().build();
        ActivityUtil.getInstance().setDb(db);

    }

    private static class AppActivityLifecycleCallbacks implements ActivityLifecycleCallbacks {
        @Override
        public void onActivityPreCreated(@NonNull Activity activity, @Nullable Bundle savedInstanceState) {
            ActivityLifecycleCallbacks.super.onActivityPreCreated(activity, savedInstanceState);
        }

        @Override
        public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle savedInstanceState) {
            ActivityUtil.getInstance().setCurrentActivity((LWActivity) activity);
        }

        @Override
        public void onActivityStarted(@NonNull Activity activity) {

        }

        @Override
        public void onActivityResumed(@NonNull Activity activity) {

        }

        @Override
        public void onActivityPaused(@NonNull Activity activity) {

        }

        @Override
        public void onActivityStopped(@NonNull Activity activity) {

        }

        @Override
        public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle outState) {

        }

        @Override
        public void onActivityDestroyed(@NonNull Activity activity) {
            ActivityUtil.getInstance().setCurrentActivity(null);

        }
    }

}
