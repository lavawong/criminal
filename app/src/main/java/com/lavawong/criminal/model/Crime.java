package com.lavawong.criminal.model;

import android.annotation.SuppressLint;
import android.text.format.DateFormat;

import java.util.Date;
import java.util.UUID;

/**
 * Crime Description
 * @author Lava Wong<wangqiang18@meituan.com>
 * @date 2021-12-24
 */
public class Crime {
    private static final String DATE_FORMAT = "EEEE, MMMM dd, yyyy";
    private String uuid;
    private String title;
    private String date;
    private boolean isSolved;
    private boolean requiresPolice;
    public Crime() {
        @SuppressLint("SimpleDateFormat")
        Date date = new Date();// 获取当前时间
        this.title = DateFormat.format(DATE_FORMAT, date).toString();
        this.uuid = UUID.randomUUID().toString();
        this.date = DateFormat.format(DATE_FORMAT, date).toString();
        this.isSolved = false;
        this.requiresPolice = false;
    }

    public Crime(String title) {
        this(title, DateFormat.format("yyyy-MM-dd HH:mm:ss a", new Date()).toString());
    }

    public Crime(String title, String date) {
        this(UUID.randomUUID().toString(), title, date, false, false);
    }

    public Crime(String uuid, String title, String date, boolean isSolved, boolean requiresPolice) {
        this.uuid = uuid;
        this.title = title;
        this.date = date;
        this.isSolved = isSolved;
        this.requiresPolice = requiresPolice;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isSolved() {
        return isSolved;
    }

    public void setSolved(boolean solved) {
        isSolved = solved;
    }

    public boolean isRequiresPolice() {
        return requiresPolice;
    }

    public void setRequiresPolice(boolean requiresPolice) {
        this.requiresPolice = requiresPolice;
    }

    public void setDate(Date date) {
        this.date = DateFormat.format(DATE_FORMAT, date).toString();
    }
}
