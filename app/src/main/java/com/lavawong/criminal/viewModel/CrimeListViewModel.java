package com.lavawong.criminal.viewModel;

import android.util.Log;

import androidx.lifecycle.ViewModel;

import com.lavawong.criminal.database.AppDatabase;
import com.lavawong.criminal.database.dao.UserDao;
import com.lavawong.criminal.database.entities.User;
import com.lavawong.criminal.model.Crime;
import com.lavawong.criminal.utils.ActivityUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CrimeListViewModel extends ViewModel {

    private static final String TAG = "CrimeListViewModel";

    private final ArrayList<Crime> crimes;

    public CrimeListViewModel() {
        crimes = new ArrayList<>();

        AppDatabase db = ActivityUtil.getInstance().getDb();

        UserDao userDao = db.userDao();
        List<User> users = userDao.getAll();
        Log.d(TAG, users.toString());
        for (int i = 0; i < 100; i++) {
            Crime crime = new Crime();
            crime.setTitle("# Title " + i);
            crime.setDate(new Date());
            if (i % 2 == 0) {
                crime.setRequiresPolice(true);
            }
            crimes.add(crime);
        }
    }

    public CrimeListViewModel(ArrayList<Crime> crimes) {
        this.crimes = crimes;
    }

    public ArrayList<Crime> getCrimes() {
        return crimes;
    }
}
